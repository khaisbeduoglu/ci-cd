import streamlit as st
import pandas as pd
import matplotlib.pyplot as plt
import sqlalchemy
import time
placeholder = st.empty()
container = st.container()
col1, col2, col3 = st.columns(3)

# Function to fetch data from the database
@st.cache_data(ttl=7)  # Cache data for 7 seconds

# Function to fetch data from the database
def fetch_data_from_db():
    engine = sqlalchemy.create_engine("mysql+mysqlconnector://root:root@localhost:8889/pchealthp")
    with engine.connect() as conn:
        df = pd.read_sql_query("SELECT * FROM system_stats", conn)
    return df

# Function to display data and plot charts
def display_data_and_plot_charts(df):
    st.title("Computer Component Health Tracker")
    
    # Display data
    st.write("### System Information")
    st.write(df)
    
    # Plot CPU Percent over time
    st.write("### CPU Percent Over Time")
    st.line_chart(df.set_index('timestamp')['cpu_percent'])
    
    # Plot Memory Percent over time
    st.write("### Memory Percent Over Time")
    st.line_chart(df.set_index('timestamp')['memory_percent'])
    
    # Plot Disk Percent over time
    st.write("### Disk Percent Over Time")
    st.line_chart(df.set_index('timestamp')['disk_percent'])

# Main function
def main():
    # Add sidebar with date slider
    selected_date = st.sidebar.date_input("Select date to view your data", value=pd.Timestamp.today())
    # Fetch data from the database
    df = fetch_data_from_db()
    # Display data and plot charts
    while True:
      df = fetch_data_from_db()
      with placeholder.container():
        filtered_df = df[df['timestamp'].dt.date == selected_date]   
        display_data_and_plot_charts(filtered_df)
      # Sleep for 7 seconds before fetching data again
      time.sleep(7)

if __name__ == "__main__":
    main()

# Tests
def test_display_data_and_plot_charts(mocker):
    # Create a sample DataFrame
    df = pd.DataFrame({
        'timestamp': ['2024-02-29 08:00:00', '2024-02-29 08:01:00'],
        'cpu_percent': [50, 60],
        'memory_percent': [30, 40],
        'disk_percent': [20, 25]
    })

    # Call the function to display data and plot charts
    result = display_data_and_plot_charts(df)

    # No assertions needed as we are not testing the return value
