# ci-cd

## Description
Week project where u get your PC's health information including CPU, MEMORY and DISK used values as percentage, send thoses datas to a Mysql Database and display them on a streamlit app.

## Getting started

### Local Installation

- Clone the project.
- Create a mysql db.
- Modifiy dataSender `get_system_info()`,  function with your database information.
- Modify app `fetch_data_from_db()` function with your database information.
- `pip install requierement.txt` to get all the libraries used in the project.
- `python3 dataSender.py` to run the script.
- `streamlit run app.py` to run the app.

***

## License
For open source projects, say how it is licensed.
