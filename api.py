from flask import Flask, render_template, jsonify
from flask_restful import Resource, Api
import mysql.connector

app = Flask(__name__)
api = Api(app)

# Function to connect to the database
def get_db_connection():
    return mysql.connector.connect(
        host="localhost",
        port="8889",
        user="root",
        password="root",
        database="pchealthp"
    )

# API Resource to get system information
class SystemInfoResource(Resource):
    def get(self):
        conn = get_db_connection()
        cursor = conn.cursor(dictionary=True)
        cursor.execute("SELECT * FROM system_stats ORDER BY timestamp DESC")
        results = cursor.fetchall()
        conn.close()

        return jsonify(results)

# API Resource to get a specific element by ID
class SystemInfoByIdResource(Resource):
    def get(self, element_id):
        conn = get_db_connection()
        cursor = conn.cursor(dictionary=True)
        cursor.execute("SELECT * FROM system_stats WHERE id = %s", (element_id,))
        result = cursor.fetchone()
        conn.close()

        if result:
            return jsonify(result)
        else:
            return jsonify({"message": "Element not found"}), 404

api.add_resource(SystemInfoResource, '/system-info')
api.add_resource(SystemInfoByIdResource, '/system-info/<int:element_id>')

if __name__ == '__main__':
    app.run(debug=True)
